===========================
Ubuntu Computer Setup Guide
===========================

Prerequisites
=============
A USB stick of capacity greater than or equal to 4GB is needed. It can be obtained from the Instrument support in PV23.

Getting Started
===============
`Ubuntu Confluence Page <https://c1.confluence.cm.volvocars.biz/display/ARTIRCA/SPA2+Virtual+Ubuntu>`_ contains information about the latest VCC Ubuntu version to be installed. It contains the link to `Ubuntu Volvo Cars <http://ubuntu.volvocars.net>`_ page. Follow instructions based on the type of OS that will be used to create the USB image. From section **Installation Media** on this page download the relevant .iso file to the local drive. 

Bootable USB
============
Windows users will be taken to the `instructions page <https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-windows>`_. Follow the instructions on this page to create the bootable USB stick.

Installation
============
Now follow **Install the machine** section on `Ubuntu Volvo Cars <http://ubuntu.volvocars.net>`_.

.. note::
	It is important to remember the Disk lock password!

After installation is finished:
	* PC restarts automatically
	* Remove usb and press *Enter* when prompted
	* Enter disk encryption password
	* Enter account username & password

After logging in, connect to a wired internet connection and disable the Network proxy settings from Network settings. Then finish downloading and installing all the updates from the Ubuntu software center. Restart the computer and then computer should be ready to use.

.. warning ::
	Not disabling the network proxy can cause block installation of some modules

.. note ::
	A lot of trouble can be avoided by *restarting* the computer

Setting up git
==============
Setup gerrit/git from `Git and Gerrit Setup Guide <http://epsdoc.cloud.volvocars.net/sphinx-doc/epshil/framework/user_doc/guidelines/GitGerritSetup.html>`_ page. 
(OR) 
If only using https then open terminal and type in the following commands:: 

	sudo apt update
	sudo apt install -y git
	# Optional
	git config --global user.name <CDSID>
	git config --global user.email your.email@volvocars.com
	git config --global core.filemode false
	git config --global core.autocrlf true

Install curl
============
Curl is required to clone the repository with the commit-message hook. It can be installed using::

	sudo apt install -y curl

Getting the Repository
======================
The repository to be cloned is ``hilmdl/vcu-pc``, available here: https://eps-sw-gerrit.volvocars.biz/admin/repos/hilmdl/vcu-pc
Open terminal and run::

	mkdir ~/hilmdl
	cd ~/hilmdl

Now clone the repo in the current directory using either *https* (password required) ::

	# clone repo here (https)
	git clone "https://CDSID@eps-sw-gerrit.volvocars.biz/a/hilmdl/vcu-pc" && (cd "vcu-pc" && mkdir -p .git/hooks && curl -Lo `git rev-parse --git-dir`/hooks/commit-msg https://CDSID@eps-sw-gerrit.volvocars.biz/tools/hooks/commit-msg; chmod +x `git rev-parse --git-dir`/hooks/commit-msg)

or using SSH if it was setup::

	# clone repo here (ssh)
	git clone "ssh://CDSID@eps-sw-gerrit-ssh.volvocars.net:29418/hilmdl/vcu-pc" && scp -p -P 29418 CDSID@eps-sw-gerrit-ssh.volvocars.net:hooks/commit-msg "vcu-pc/.git/hooks/"

Fetch any non merged changes (if any required). Either checkout the fetch head or create a new branch and checkout the change.

Run the setup script
====================
Before continuing make sure there is no background updates running as it can interfere with the setup script. To execute the setup run the following commands in terminal::

	cd ~/hildml/vcu-pc/computer-setup
	exec bash
	source main-script
	# (Follow instructions)
	# Choose <Yes> for wireshark prompt

If everything is installed and configured as intended, there should not be any errors in the console log. Check the log to see if there was any specific problem. **Restart** the computer for all the changes to take effect. In case of any errors check the next section. 

.. warning ::
	Some modules might not work properly if restart is not completed

Error handling
==============
Manual uninstall or inspect the cause behind the erroneous module. Open ``~/hilmdl/vcu-pc/computer-setup/installed`` file and remove the lines containing the relevant modules e.g. ``ssh_installed=1``, which had problems with installation. Common errors are due to permissions or internet settings. Then save the file and execute following commands::
	
	cd ~/hildml/vcu-pc/computer-setup
	exec bash
	source main-script
	# (Follow instructions)


f5fpc VPN (Optional)
====================
To enable wireless connection PointSharp PIN and OTP is required which has to be setup for a given CDSID. Following links provide more information:

* Blog on Volvo VPN : Linux (f5vpn, f5fpc), Windows, Mac `F5 VPN <https://mysite.volvocars.net/personal/nkorkaka_volvocars_com/Blog/Lists/Posts/Post.aspx?List=dcf8efad%2D8f05%2D4847%2D8414%2Dfee7545986d7&ID=14&Web=0f6ade52%2Db647%2D4631%2D94ae%2D2bb334f5acf6>`_
* Installing and using `Pointsharp <https://intranet.volvocars.net/:p:/r/sites/SupportVcg/_layouts/15/Doc.aspx?sourcedoc=%7BB8B41401-02A8-4CFA-9917-395BEBDDA4F4%7D&file=Installing%20and%20using%20Pointsharp.ppsx&action=edit&mobileredirect=true&DefaultItemOpen=1>`_
* A similar `doc <https://intranet.volvocars.net/:w:/r/sites/apacit/ED%20APAC/IT%20DesignRDME/CADPLMME/_layouts/15/Doc.aspx?sourcedoc=%7BC5C7D39D-1FF8-4CE0-8E7C-4A13E31B1456%7D&file=VPN%20setup%20using%20PointSharp.doc&action=default&mobileredirect=true&DefaultItemOpen=1>`_

For regular connection to wireless network::

	# Create vpn-login file
	touch ~/vpn-login
	echo "sudo f5fpc --start -t https://iconnect.global.volvocars.biz" > vpn-login
	source vpn-login
	# Enter sudo pwd
	# Enter cdsid and pwd

Check the connection status using::

	f5fpc --info

.. note::
	Password for VPN is different from CDSID password
