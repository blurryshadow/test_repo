// Declarative //

pipeline {

	agent {
		label "condor"
	}

	stages {
		stage('Print') {
			steps {
				powershell 'python -V'
				powershell 'pwd'
			}
		}

		stage('Run') {
			steps {
				powershell 'python ./src/main.py'
			}
		}
	}

	post {
		always {
			echo 'This will always run :D'
		}
		success {
			echo 'This will run only if successful'
		}
		failure {
			echo 'This will run only if failed'
		}
		unstable {
			echo 'This will run only if the run was marked as unstable'
		}
		changed {
			echo 'This will run only if the state of the Pipeline has changed'
			echo 'For example, if the Pipeline was previously failing but is now successful'
		}
    }
}