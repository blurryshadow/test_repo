// <h2><span style="color:green;">Sphinx gate</span></h2>
// A <b>sphinx</b> gate for <i>stricter</i> checking of documentation. <br>
// To trigger this build manually from Gerrit, press the <b>Reply</b> button and type <b>verifyfiles!</b>

pipeline {

	agent {
		label "ubuntu_db"
	}

	options {
		timestamps()
	}

	parameters {
		string(name: 'IMAGE_EXT', defaultValue: 'PNG,png,jpg,jpeg,svg,ipe,isy', description: '<b>Image extensions</b> allowed in documentation <br> NOTE: To be specified as commma separated values (no spaces) <br> Ex: png,jpg,svg')
		string(name: 'FILE_EXT', defaultValue: 'rst,css,txt,bat,lua,m,c,cpp,py', description: '<b>File extensions</b> allowed in documentation <br> NOTE: To be specified as commma separated values (no spaces) <br> Ex: rst,css,txt')
		string(name: 'MAX_FILE_SIZE', defaultValue: '2M', description: '<b>File size limit</b> allowed in documentation <br> NOTE: To be specified as number followed by units <br> Ex: 10k, 2M, 1024c')
	}

	environment {
		VENV_DIR_NAME = 'build_env'
		SCRIPTS_DIR = "${WORKSPACE}/src"
		REPO_DIR = 'ws_repo'
	}

	stages {
		stage('Checkout'){
			steps {
				sh "rm -rf ${REPO_DIR}"
				dir("${REPO_DIR}"){
					echo "Checking out: ${GERRIT_PROJECT}/${GERRIT_REFSPEC}"
					checkout([
						$class: 'GitSCM',
						branches: [[name: "${GERRIT_REFSPEC}"]],
						doGenerateSubmoduleConfigurations: false, // This seems important based on internet research, but no idea what it does...
						extensions: [[$class: 'CleanBeforeCheckout'], 
									 [$class: 'CleanCheckout']],
						userRemoteConfigs: [
							[
								credentialsId: 'f7e97355-4aaf-485d-95b8-85a7617d4092',
								refspec: "+${GERRIT_REFSPEC}:refs/remotes/origin/${GERRIT_REFSPEC}",
								url: "ssh://10.40.182.21:29418/${GERRIT_PROJECT}"
								// url: "ssh://10.40.138.238:29418/${GERRIT_PROJECT}"
							]
						]
					])
				}
			}
		}

		stage('Environment') {
			steps {
				sh '''#!/bin/bash
					rm -rf ${VENV_DIR_NAME}
					python3 -m venv ${VENV_DIR_NAME}
					. ${VENV_DIR_NAME}/bin/activate
					python -V
					python -m pip install --upgrade pip
					python -m pip install --upgrade wheel
					if [[ -e "${REPO_DIR}/doc/requirements.txt" ]]; then
						python -m pip install --upgrade -r "${WORKSPACE}/${REPO_DIR}/doc/requirements.txt"
					elif [[ -e "${REPO_DIR}/doc/source/requirements.txt" ]]; then
						python -m pip install --upgrade -r "${WORKSPACE}/${REPO_DIR}/doc/source/requirements.txt"
					else
						python -m pip install --upgrade -r "${SCRIPTS_DIR}/requirements.txt"
					fi
					python -V
					deactivate
				'''
			}
		}

		stage('Structure') {
			steps{
				dir("${REPO_DIR}/doc"){
					sh '''#!/bin/bash
						. "${WORKSPACE}/${VENV_DIR_NAME}/bin/activate"
						# Run the file type ans structure check script
						source "${SCRIPTS_DIR}/struct_check.sh"
						deactivate
					'''
				}
			}
		}

		stage('File Size') {
			steps{
				dir("${REPO_DIR}/doc"){
					sh '''#!/bin/bash
						. "${WORKSPACE}/${VENV_DIR_NAME}/bin/activate"
						# Run the size check script
						source "${SCRIPTS_DIR}/size_check.sh"
						deactivate
					'''
				}
			}
		}

		stage('Build') {
			steps {
				dir("${REPO_DIR}/doc") {
					sh '''
						. "${WORKSPACE}/${VENV_DIR_NAME}/bin/activate"
						pwd
						python -V
						python -m sphinx --version
						make clean
						make html SPHINXOPTS="-E -W --keep-going"

						deactivate
					'''
				}
			}
		}
	}

	post {
		always {
			echo "Pipeline ${JOB_BASE_NAME} ran on ${NODE_NAME} agent with Build Number ${BUILD_NUMBER}"
		}
		success {
			echo "Pipeline run successful for ${GERRIT_PROJECT} repository"
		}
		failure {
			echo "Pipeline run failed for ${GERRIT_PROJECT} repository"
		}
		unstable {
			echo "There were some unstabilities for ${GERRIT_PROJECT} repository"
		}
	}
}