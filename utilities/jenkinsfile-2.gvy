// Declarative //

pipeline {

	agent {
		label "ubuntu1804"
	}

	environment {
		VENV_DIR_NAME = 'test_env'
	}

	stages {
		stage('Print') {
			steps {
				timeout(time: 2, unit: 'MINUTES') {
					sh 'python -V'
					sh 'python3 -V'
				}
			}
		}

		stage('Environment') {
			steps {
				sh '''
					python3 -m venv ${VENV_DIR_NAME}
				 	. ${VENV_DIR_NAME}/bin/activate
				 	python -m pip install --upgrade wheel
				 	python -m pip install --upgrade -r src/requirements.txt
					python -V
				'''
			}
		}

		stage('Build') {
			steps {
				sh '''
					. ${VENV_DIR_NAME}/bin/activate
					pwd
					cd docs
					pwd
					echo "Who I'm $SHELL"
					python -m sphinx --version
					make clean
					make html
					cd ..
					pwd
				'''
			}
		}

		stage('Run') {
			steps {
				sh 'python3 ./src/main.py'
			}
		}
	}

	post {
		always {
			echo 'This will always run'
		}
		success {
			echo 'This will run only if successful'
		}
		failure {
			echo 'This will run only if failed'
		}
		unstable {
			echo 'This will run only if the run was marked as unstable'
		}
		changed {
			echo 'This will run only if the state of the Pipeline has changed'
			echo 'For example, if the Pipeline was previously failing but is now successful'
		}
    }
}