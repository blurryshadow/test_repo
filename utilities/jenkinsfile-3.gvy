// Gate for checeking sphinx build and deploy it to some remote locations

pipeline {

	agent {
		label "ubuntu_db"
	}

	options {
		timestamps()
	}

	environment {
		VENV_DIR_NAME = 'build_env'
		DEPLOY_LOCAL = 0
		DEPLOY_EPSDOC = 0
	}

	stages {

		stage('Checkout'){
			steps {
				sh 'rm -rf repo'
				dir('repo'){
					echo "Branch name: ${GERRIT_REFSPEC}"
					echo "Project name: ${GERRIT_PROJECT}"
					echo "Workspace: ${WORKSPACE}"
					echo "Jobname: ${JOB_NAME}"
					checkout([
						$class: 'GitSCM',
						branches: [[name: "${GERRIT_REFSPEC}"]],
						doGenerateSubmoduleConfigurations: false, // This seems important based on internet research, but no idea what it does...
						extensions: [[$class: 'CleanBeforeCheckout'], 
									 [$class: 'CleanCheckout']],
						userRemoteConfigs: [
							[
								credentialsId: 'f7e97355-4aaf-485d-95b8-85a7617d4092',
								refspec: "+${GERRIT_REFSPEC}:refs/remotes/origin/${GERRIT_REFSPEC}",
								url: "ssh://10.40.182.21:29418/${GERRIT_PROJECT}"
								// url: "ssh://10.40.138.238:29418/${GERRIT_PROJECT}"
							]
						]
					])
				}
			}
		}

		stage('Environment') {
			steps {
				sh '''#!/bin/bash
					rm -rf ${VENV_DIR_NAME}
					python3 -m venv ${VENV_DIR_NAME}
					. ${VENV_DIR_NAME}/bin/activate
					python -m pip install --upgrade pip
					python -m pip install --upgrade wheel
					if [[ -e repo/doc/requirements.txt ]]; then
						python -m pip install --upgrade -r "${WORKSPACE}/repo/doc/requirements.txt"
					elif [[ -e repo/doc/source/requirements.txt ]]; then
						python -m pip install --upgrade -r "${WORKSPACE}/repo/doc/source/requirements.txt"
					else
						python -m pip install --upgrade -r "${WORKSPACE}/src/requirements.txt"
					fi
					python -V
					deactivate
				'''
			}
		}

		stage('Build') {
			steps {
				dir('repo/doc') {
					sh '''
						set -x
						. "${WORKSPACE}/${VENV_DIR_NAME}/bin/activate"
						pwd
						python -V
						echo ${SHELL}
						python -m sphinx --version
						make clean
						make html SPHINXOPTS="-E --keep-going"
						deactivate
					'''
				}
			}
		}

		stage('Deliver') {
			steps {
				echo "start ..."
				script {
					if ("${DEPLOY_EPSDOC}" == '1') {
						withCredentials([sshUserPrivateKey(
												credentialsId: "f7e97355-4aaf-485d-95b8-85a7617d4092",
												keyFileVariable: "SSH_KEY_FILE",
												usernameVariable: "SSH_USERNAME")]) {
								sh '''#!/bin/bash
									DESTINATION=/data/www/html/sphinx-doc/$GERRIT_PROJECT/
									REMOTE=epspmt@epsci-lt01.cloud.volvocars.net
									SSH_COMMAND="ssh -o StrictHostKeyChecking=no -i $SSH_KEY_FILE"

									$SSH_COMMAND $REMOTE mkdir -p $DESTINATION
									rsync -avre \"$SSH_COMMAND\" repo/doc/build/html/ $REMOTE:$DESTINATION
								'''
							}
					}
					if ("${DEPLOY_LOCAL}" == '1') {
						withCredentials([sshUserPrivateKey(
												credentialsId: "42395329-8fcd-4794-aa03-58aabc48045c",
												keyFileVariable: "SSH_KEY_FILE",
												usernameVariable: "SSH_USERNAME")]) {
								sh '''#!/bin/bash
									DESTINATION=/home/epshil/jenkins_ws/deployed/docs/
									REMOTE=$SSH_USERNAME@10.246.47.247
									SSH_COMMAND="ssh -o StrictHostKeyChecking=no -i $SSH_KEY_FILE"

									$SSH_COMMAND $REMOTE mkdir -p $DESTINATION
									rsync -avre \"$SSH_COMMAND\" repo/doc/build/html/ $REMOTE:$DESTINATION
								'''
							}
					}
				}
				echo "... end"
			}
		}
	}

	post {
		always {
			echo "Pipeline ${JOB_BASE_NAME} ran on ${NODE_NAME} agent with Build Number ${BUILD_NUMBER}"
		}
		success {
			echo 'Pipeline run successful'
		}
		failure {
			echo 'Pipeline run failed on ${NODE_NAME}'
		}
		unstable {
			echo 'The run was marked as unstable'
		}
	}
}