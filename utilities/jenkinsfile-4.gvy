// For finding VCU processor versions

pipeline {

	agent {
		// label "ubuntu_cd"
		label "${AGENT}"
	}

	parameters {
		choice(name: 'PROCESSOR', choices: 'SGA\nHPA\nLPA\nHIA\nHIB', description: 'Choose the processor')
		choice(name: 'AGENT', choices: 'ubuntu_cd\nubuntu_db', description: 'Choose the agent')
		booleanParam(name: 'VERBOSE', defaultValue: false, description: 'Print verbose output (Not required)')
	}

	stages {
		stage('Environment') {
			steps {
				timeout(time: 2, unit: 'MINUTES') {
					sh 'python3 -V'
				}
			}
		}

		stage('Hardware') {
			steps {
				sh'''#!/bin/bash
					FILE=/home/epshil/hilmdl/vcu-pc/vcu-control/listUSBDevices.sh
					RESULT=$(bash "$FILE" | grep ttyUSB0 | grep FTDI | wc -l)
					if [[ $RESULT = 1 ]]
					then
						echo "Correct UART setup"
					else
						echo "Wrong UART setup"
						exit 1
					fi
				'''
			}
		}

		stage('Version') {
			steps {
				sh'''#!/bin/bash
					if [[ "$AGENT" = "ubuntu_cd" ]]; then
						agent="Chargining Domain Host PC"
					elif [[ "$AGENT" = "ubuntu_db" ]]; then
						agent="Download Bench Host PC"
					fi

					if $VERBOSE; then
						set -x
						python3 src/com.py -v -p "${PROCESSOR}"
					else
						python3 src/com.py -p "${PROCESSOR}"
					fi

					echo "Machine: ${agent}"
				'''
			}
		}
	}
}