#! /bin/bash

# Import settings
source "${SCRIPTS_DIR}/config.sh"

# Image extensions
image_ext=$(sed 's/,/|\\./g' <<< "${IMAGE_EXT}")
image_ext="\.$image_ext"
# image_ext='\.PNG|\.png|\.jpg|\.jpeg|\.svg|\.ipe|\.isy'

# File extensions
file_ext=$(sed 's/,/|\\./g' <<< "${FILE_EXT}")
file_ext="\.$file_ext"
# file_ext='\.rst|\.css|\.txt|\.bat|\.lua|\.m|\.c|\.cpp|\.py'

args=""
for path in "${ignore_dirs[@]}"
do
	args="${args}${base_dir}/${path}|"
done

# Find all image folders
all_image_dirs=()
for dir in "${image_dir_names[@]}"
do
	if [[ -z "${args}" ]]; then
		_image_dirs=$(find $base_dir -type d -name $dir)
	else
		grep_args=${args::-1}
		_image_dirs=$(find $base_dir -type d -name $dir | egrep -v $grep_args)
	fi
	all_image_dirs=(${all_image_dirs[@]} ${_image_dirs[@]})
done

# Check only image files in image folders
num_images_good=0
for path in "${all_image_dirs[@]}"
do
	num_right_files=$(find "${path}" -type f | egrep $image_ext | wc -l)
	num_wrong_files=$(find "${path}" -type f | egrep -v $image_ext | wc -l)
	num_images_good=$(($num_images_good + $num_right_files))
	if [[ $num_wrong_files -gt 0 ]]
	then
		printf "${RED}\nWrong image file type in:\n${NC}"
		printf "${path}\n"
		exit 1
	fi
done

# No image files outside images folder
if [[ -z "${args}" ]]; then
	num_images_all=$(find "${base_dir}" -type f | egrep $image_ext | wc -l)
else
	grep_args=${args::-1}
	num_images_all=$(find "${base_dir}" -type f | egrep $image_ext | egrep -v $grep_args | wc -l)
fi
if [[ $num_images_all -ne $num_images_good ]]; then
	printf "${RED}\nSome image files outide images folders!\n${NC}"
	exit 1
fi

# Only code files in non-image folders
# Exception for Makefile
combined_ext="${image_ext}|${file_ext}"
if [[ -z "${args}" ]]; then
	wrong_files_all_temp=$(find "${base_dir}" -type f | egrep -v $combined_ext | grep -v 'Makefile')
else
	grep_args=${args::-1}
	wrong_files_all_temp=$(find "${base_dir}" -type f | egrep -v $combined_ext | egrep -v $grep_args | grep -v 'Makefile')
fi
IFS=$'\n' read -rd '' -a wrong_files_all <<< "$wrong_files_all_temp"
num_files_all="${#wrong_files_all[@]}"
if [[ $num_files_all -gt 0 ]]; then
	printf "${RED}\nWrong code/text files types found (${num_files_all}):\n${NC}"
	printf "${wrong_files_all_temp}\n"
	exit 1
fi

echo "All good!"
