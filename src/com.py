import serial
import subprocess
import argparse
import time
import os


version_cmds = {
	'SGA': [b'\n', b'cat /opt/versions\n', b'$'],
	'HPA': [b'\n', b'cat /usr/libnvidia/version-pdk.txt\n', b'#'],
	'LPA': [b'\r', b'version\r', b'->'],
	'HIA': [b'\n', b'version\n', b'>'],
	'HIB': [b'\n', b'version\n', b'>']
}

match_txt = {
	'SGA': 'build',
	'HPA': 'cat ',
	'LPA': 'version',
	'HIA': 'SW Version',
	'HIB': 'SW Version'
}


def extract_version(out_str, processor):
	out_str = out_str.replace('\r\n', '\n')
	out_lines = out_str.split('\n')
	version = 'NULL'
	for i, line in enumerate(out_lines):
		if match_txt[processor] in line:
			if processor in ['LPA', 'HPA']:
				version = out_lines[i+1]
			else:
				version = line.split()[-1]
			break
	return version



if __name__ == "__main__":

	ap = argparse.ArgumentParser()
	ap.add_argument('-p', '--processor', required=True, help="Processor name in capital letters")
	ap.add_argument('-v', '--verbose', action='store_true')
	args = ap.parse_args()

	PROC = args.processor

	comport = serial.Serial('/dev/ttyUSB0', 115200, timeout=1)

	if comport:
		subprocess.run(['python3', '/home/epshil/hilmdl/vcu-pc/vcupc/main.py', '-p', f'{PROC}'], stdout=open(os.devnull, 'wb'))

		comport.reset_input_buffer()
		comport.reset_output_buffer()

		# newline		
		comport.write(version_cmds[PROC][0])
		out = comport.read()

		# version command
		comport.write(version_cmds[PROC][1])
		pre_out = comport.read_until(version_cmds[PROC][2])
		out = comport.read_until(version_cmds[PROC][2])
		# print('Pre-Output:', pre_out.decode('ascii'))
		out_str = out.decode('ascii')
		if args.verbose:
			print('Output:', out_str)
			print('-'*30)

		version = extract_version(out_str, PROC)
		print(f'{PROC} version: {version}')

		comport.close()
		del comport

	else:
		print('Cannot access Processor')
