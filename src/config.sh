#! /bin/bash

base_dir='.'

GREEN="\033[0;32m"
RED="\033[0;31m"
NC="\033[0m"

# List of folders to ignore
ignore_dirs=(
'_build'
'_static'
'_templates'
)

# List of names for allowed image folders
image_dir_names=(
'images'
'_images'
)

GREEN=""
RED=""
NC=""