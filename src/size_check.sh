#! /bin/bash

# Import settings
source "${WORKSPACE}/src/config.sh"

# File size limitation
# Ignore build files
large_files_temp=$(find "${base_dir}" -type f -size +"$MAX_FILE_SIZE" | egrep -v '_build|build')
IFS=$'\n' read -rd '' -a large_files <<< "$large_files_temp"
num_large_files="${#large_files[@]}"
if [[ $num_large_files -gt 0 ]]; then
	printf "${RED}\nFiles larger than allowed size ($MAX_FILE_SIZE) found (${num_large_files}):\n${NC}"
	printf "${large_files_temp}\n"
	exit 1
else
	echo "All good!"
	exit 0
fi